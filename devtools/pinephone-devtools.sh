#!/bin/sh

GADGET_RO=

# Mount eMMC read-only if we're booting from it (or bad things will happen)
ROOTDEV=`mount | grep "on / " | cut -d ' ' -f 1`
if [ "$ROOTDEV" = "/dev/mmcblk2p1" ]; then
    GADGET_RO="ro=1"
fi

modprobe g_multi file=/dev/mmcblk2 stall=0 removable=1 $GADGET_RO \
                 host_addr=fe:ed:de:ad:be:ef
